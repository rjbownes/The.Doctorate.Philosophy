# Hi and welcome!

## Introduction 

I am a PhD Student of Bioinformatics at the University of Edinburgh and I am going to be compiling my work and side learning over the next few months as I transition from academia to my first job as a data scientist. I work primarily in R, so I will be show casing how I go about learn object orient programming in python. I'm enrolled in the Stanford Machine Learning course by Andrew Ng and will keep a copy of my work here, as well as solutions and explanations for Bioinformatics problems hosted on Rosalyn and Euclid.

## Other files.

I have also placed my dot files for nvim/tmux and a few other applications here incase any one is looking for a starting place to building their own vim/um or tiling window manager on OSX.

