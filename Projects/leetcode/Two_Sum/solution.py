class Solution:
    #this is an attempt to solve problem #1 in codeleet
    #Retrun the index of the numbers in an array that
    #sum to the target value. 
    def twoSum(self, nums, target):
        sums = [] # create an empty array to hold the sums
        hashTable = {} # create an empty hashtable
        for i in range(0, len(nums)): # check each element of the array.
            missingValue = target - nums[i] # create a variable that is equal to
                                         # difference between the target and
                                         # the value at that i'th index of the
                                         # array.
            if missingValue in hashTable: #check if the value of missingValue
                                          #is in the hashtable, if it is then
                                          # we have found the pair!
                sums.append([nums[i], missingValue]) #append the pair into sums

            hashTable[nums[i]] = nums[i] #add the current value to the
                                         # if the value is not the match
        return [nums.index(sums[0][1]), nums.index(sums[0][0])]

# Now lets test this
test = Solution()
print(test.twoSum([3,5,7,9,2,6], 16))
