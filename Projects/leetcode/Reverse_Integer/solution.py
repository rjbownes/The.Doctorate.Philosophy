#Create a class "solution".
#With a method "reverse".
#convert int to str
#reverse the string
#convert back to int
class Solution:
    def reverse(self, x):
        return int(str(x)[::-1])
