---
title: "Week 1. Chapter 1"
author: "Richard Bownes"
date: "`r format(Sys.Date())`"
output:
  github_document
---
# The Golem of Prague

McElreath opens this book with a parabol of the golem of prague, and relates the mindless operation of a clay golem to the irresponsibly sourced models of todays research scientists and academics, to paraphrase:

> Scientists also make golems ... These golems are scientific models. But these golems have real effects on the world, through the predictions they make and the intuitions they challenge or inspire. A concern with truth enlivens these models, but just like a golem or a modern robot, scientific models are neither true nor false, neither prophets nor charlatans. Rather they are constructs engineered for some purpose. These constructs are incredibly powerful, dutifully conducting their programmed calculations.

This is a lot like the old adage *All models are wrong, but some are useful* in that we as researchers can employ models all day long, but the wrong model, or the wrong golem will never be the correct tool and may lead to unexpected or unwanted answers. 

## Summary {-}

Chapter 1 of **Statistical Rethinking** contains no code but is definitly worth reading for a reminder of the unchecked power of statistical models for drawing inference that may have unforseen consequences. 

>Rather than idealized angels of reason, scientific models are powerful clay robots without intent of their own, bumbling along according to the myopic instructions they embody.

Additionally, and of special interest is McElreaths rebuttal of Karl Poppers assertions on the necessary falsification of hypothesis. This is due to the following two points illustrating that all deductive falsification is nearly impossible for all scientific undertakings. 

> 1. Hypotheses are not models. The relations among hypotheses and different kinds of
models are complex. Many models correspond to the same hypothesis, and many
hypotheses correspond to a single model. This makes strict falsification impossible.

> 2. Measurement matters. Even when we think the data falsify a model, another ob-
server will debate our methods and measures. They don’t trust the data. Sometimes they are right.

The scientific method, therefore, can not be reduced to a single statistical procedure, so we should not pretend our models are able to! This isn't to say scientific methods don't work, of course they do, and wonderfully, just not necessarily through falsification. Especially not falsificatin of a **NULL** hypothesis!

BDA may sounds confusing, but it is just glorified counting. Sum up all of the possible ways a thing can occur given some conditions, and all the the ways another thing can occur. The one with the higher "count" is of higher probability and more likely to occur. This is a natural extension of ordinary logic  applied to continuous *plausibility*. In this wasy BDA doesn't attempt to falsify a null hypothesis to prove a point instead it compares similar and **meaningful** models to infer plausibility. These will all be illustrated in the following chapter sections. 




