#In this this script I will attempt to use matplotlib to make a barplot.

import matplotlib.pyplot as plt
from sklearn import datasets
import pandas as pd
import numpy as np
#Load iris and extract and transpose the feature data.
iris = datasets.load_iris()
df = pd.DataFrame(iris.data, columns=iris.feature_names)

#create a data frame to iterate over and find our values for each feature.
df['target']=iris.target
df['species'] = df['target'].map({0:iris.target_names[0],
                                  1:iris.target_names[1],
                                  2:iris.target_names[2]})

#write a for loop to gather the max values to plot with.
appended_data = []
for i in iris.target_names:
    x=df.loc[(df.species==i)]
    max_values=pd.DataFrame(x.max())
    appended_data.append(max_values)

#print was  only used to identify the max values
#print(appended_data)

y_pos = np.arange(len(iris.target_names))
performance = [5.8, 7, 7.9]

plt.bar(y_pos,
        performance,
        align='center',
        alpha=.2,
        color =['red', 'green', 'blue'])
plt.xticks(y_pos, iris.target_names)
plt.ylabel('sepal length (cm)')
plt.title("Sepal Length across flower types")
plt.savefig("../output/barplot.png")

