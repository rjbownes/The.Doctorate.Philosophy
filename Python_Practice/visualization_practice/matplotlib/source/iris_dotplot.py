# So this is the first of my visualization practice scripts. I am intentionally
# not using jupyter notebooks intentionally because I want to practive in VIM,
# and I want to eventually move to modular python architecture.

#matplot lib is for plotting, sklearn is only for for the iris dataset.
import matplotlib.pyplot as plt
from sklearn import datasets


# The iris data set has 5 features, and a target catagory. Here we are
# extracting only the feature data, and transforming it to be 5 arrays.
iris = datasets.load_iris()
features = iris.data.T

#plt.scatter() is the same thing as a dot plot, x and y are explitict as the
#first two arguments. Alpha is transparency, s is the size of the point. and is
# the major differentiator between plt.plot and plt.scatter.
plt.scatter(features[0],
            features[1],
            s=100*features[3],
            alpha=0.2,
            c=iris.target,
            cmap='viridis')
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.savefig('../output/scatter.png')
