#In this script I will attempt to make an attractive box plot using matplotlib
#to highlight differences in the iris data set. 
import matplotlib.pyplot as plt
from sklearn import datasets

iris = datasets.load_iris()
features = iris.data.T


#matplotlib offers little control over how the box plot functions because this
#object in an np.ndarray. The most appropriate way to plot box plots I think is
#to make the data into a pandas dataframe and plot with that function instead.
import pandas as pd

df = pd.DataFrame(iris.data, columns=iris.feature_names)


df['target']=iris.target

df['species'] = df['target'].map({0:iris.target_names[0],
                                  1:iris.target_names[1],
                                  2:iris.target_names[2]})

#pandas plot cannot directly save through plt.savefig

fig = df.boxplot(column=list(df)[0:4], by='species')
fig.savefig("../output/boxplot.png")
